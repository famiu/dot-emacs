;; -*- lexical-binding: t -*-
;; Make sure Emacs config directory is the parent directory of the directory of this file

;; Load required libs
(require 'subr-x)

;; Set base name of script for later usage
(setq config--script-basename
      (file-name-base (expand-file-name load-file-name)))

(defun config-setup--help (&rest args)
  "Shows the help for this script."
  (let ((cmds) (maxwidth 0))
    (mapatoms
     (lambda (sym)
       (when-let (((string-prefix-p "config-setup--" (symbol-name sym)))
                  (cmd-name (string-remove-prefix "config-setup--" (symbol-name sym))))
         (add-to-list 'cmds cmd-name))))

    (setq maxwidth (apply #'max (mapcar #'length cmds)))

    (message "Usage: %s [command]\n\nCommands:" config--script-basename)

    (mapcar
     (lambda (elem)
       (message (format (concat "  %-" (number-to-string maxwidth) "s : %s")
                        elem (elisp--docstring-first-line
                              (documentation (intern (format "config-setup--%s" elem)))))))
     cmds)))

(defun config-setup--install (&rest args)
  "Sets up the Emacs config and installs all necessary plugins."
  (setq user-emacs-directory (expand-file-name "../../" load-file-name))

  ;; Load the configurations
  (load (concat user-emacs-directory "early-init") nil t)
  (load (concat user-emacs-directory "init") nil t)

  ;; Run the after-init hook to evaluate the eval-after-init blocks
  (run-hooks 'after-init-hook))

(defun config-setup--clean (&rest args)
  "Cleans unused packages."
  (require 'straight-x)
  (straight-x-clean-unused-repos))

(defun config-setup--update (&rest args)
  "Updates all existing packages."
  (straight-pull-all))

(defun config-setup--rebuild (&rest args)
  "Rebuilds all existing packages."
  (straight-rebuild-all))

(unless argv
  (error "No arguments provided! Please run '%s help' for help."
         config--script-basename))

(let ((arg (pop argv)))
  (if-let ((func (intern-soft (format "config-setup--%s" arg))))
      (apply func argv)
    (error
     "Invalid argument: '%s'\nPlease run '%s help' for help."
     arg config--script-basename)))
