;; Initial and default frame settings
(setq initial-frame-alist
      (append initial-frame-alist
              '((top 0.5)
                (left 0.5)
                (width . 140)
                (height . 40))))

(setq default-frame-alist
      (append default-frame-alist
              '((top 0.5)
                (left 0.5)
                (width . 140)
                (height . 40))))

;; Line numbers
(setq display-line-numbers-type 'relative)
