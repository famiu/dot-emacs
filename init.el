;; -*- lexical-binding: t -*-
;; Configuration for Emacs

;; Load the config base
(load (concat user-emacs-directory "base/base") nil t)

;; Load the modules
(load-modules!
 :editor
 evil

 :ui
 theme

 :langs
 elisp
 )

;; Load the post-module configuration
(condition-case err
    (load (concat config-dir "config") nil t)
  ('error
   (error "Error while loading user config:\n%s" err)))


