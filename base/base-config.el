;; -*- lexical-binding: t -*-
;; Sane defaults and settings for Emacs.

;; Disable startup screen
(setq inhibit-startup-screen t)

;; Emacs file load settings
(setq load-prefer-newer t)

;; Clipboard, selection and kill-ring settings
(setq
 kill-do-not-save-duplicates t
 select-enable-clipboard nil
 select-enable-primary t
 select-active-regions t
 mouse-drag-copy-region t)

;; Indent
(setq-default
 indent-tabs-mode nil
 tab-width 4
 tab-always-indent nil)

;; Make `tabify' and `untabify' only affect indentation.
(setq tabify-regexp "^\t* [ \t]+")

;; Fill column
(setq-default fill-column 100)
(add-hook 'prog-mode-hook
          (lambda () (display-fill-column-indicator-mode)))

;; Line wrap
(setq-default
 word-wrap t
 truncate-lines t)

(setq truncate-partial-width-windows nil)

;; Single space ends sentences
(setq sentence-end-double-space nil)

;; Make files end in newline
(setq require-final-newline t)

;; Scroll margin
(setq scroll-margin 10)

;; History file
(setq savehist-file (concat config-cache-dir "savehist"))

;; Backup file
(setq
 create-lockfiles t
 make-backup-files nil
 version-control t
 backup-by-copying t
 delete-old-versions t
 kept-old-versions 3
 kept-new-versions 3
 backup-directory-alist (list (cons "." (concat config-cache-dir "backup/")))
 tramp-backup-directory-alist backup-directory-alist)

;; Autosave
(setq
 auto-save-default t
 auto-save-interval 50
 auto-save-include-big-deletions t
 auto-save-list-file-prefix (concat config-cache-dir "autosave/")
 tramp-auto-save-directory  (concat config-cache-dir "tramp-autosave/")
 auto-save-file-name-transforms
 (list (list "\\`/[^/]*:\\([^/]*/\\)*\\([^/]*\\)\\'"
	     ;; Prefix tramp autosaves to prevent conflicts with local ones
	     (concat auto-save-list-file-prefix "tramp-\\2") t)
       (list ".*" auto-save-list-file-prefix t)))

;; Use soft line-wrapping in text modes
(add-hook 'text-mode-hook #'visual-line-mode)

;; Auto-modes
(nconc
 auto-mode-alist
 '(("/LICENSE\\'" . text-mode)
   ("\\.log\\'" . text-mode)
   ("rc\\'" . conf-mode)
   ("\\.\\(?:hex\\|nes\\)\\'" . hexl-mode)))

;; Persist variables across sessions
(use-package savehist
  :init
  (setq savehist-file (concat config-cache-dir "savehist"))
  :config
  (savehist-mode 1))

;; Emacs confirm on kill
(setq confirm-kill-emacs #'yes-or-no-p)

;; Use the short-query function for yes-no questions
(defalias 'yes-or-no-p #'y-or-n-p)

(provide 'base-config)
