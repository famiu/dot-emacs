;; Define directory variables
(defconst config-dir user-emacs-directory
  "Location of Emacs config.")

(defconst config-base-dir (concat config-dir "base/")
  "Directory where the essential configuration files are stored.")

(defconst config-modules-dir (concat config-dir "modules/")
  "Directory where configuration modules are stored.")

(defconst config-local-dir (concat config-dir ".local/")
  "Directory where data is stored.")

(defconst config-cache-dir (concat config-local-dir "cache/")
  "Cache directory.")

;; Add base and modules directories to load path
(add-to-list 'load-path config-base-dir)
(add-to-list 'load-path config-modules-dir)

;; Load required libraries
;; Common-LISP emulation
(require 'cl-lib)

;; Load Utilities
(require 'base-utils)

;; Load package management system
(require 'base-package)

;; Load modules loader
(require 'base-modules)

;; Load configurations
(require 'base-config)

;; Load base editor configuration
(require 'base-editor)

;; Load base UI configuration
(require 'base-ui)

;; Load base keybind configuration
(require 'base-keybinds)

(provide 'base)
