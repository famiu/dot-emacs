;; -*- lexical-binding: t -*-
;; Initialize the package management system using straight.el

;; Straight settings
(setq straight-repository-branch "develop")
(setq straight-use-package-by-default t)
(setq straight-cache-autoloads t)

;; Bootstrap straight.el if it doesn't exist
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; Get use-package
(straight-use-package 'use-package)

;; Prune build and remove all unused packages after init is loaded
(eval-after-init!
 (require 'straight-x)
 (straight-prune-build)
 (straight-x-clean-unused-repos))

(provide 'base-package)
