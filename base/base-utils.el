;; -*- lexical-binding: t -*-
;; Utilities for the Emacs config

(defmacro add-hook-run-once! (hook func &optional depth local)
  "Like `add-hook', but the function runs only once."
  (let ((sym (gensym "#run-once")))
    `(progn
       (defun ,sym ()
         (remove-hook ,hook ',sym ,local)
         (funcall ,func))
       (add-hook ,hook #',sym ,depth ,local))))

(defmacro eval-on-hook! (hook &rest body)
  "Evaluate BODY when HOOK is run.
If the second argument is `once', runs BODY only once."
  (declare (indent defun))
  (if (not (equal (car body) 'once))
      `(add-hook ',hook (lambda () ,@body))
    `(add-hook-run-once! ',hook (lambda () ,@(cdr body)))))

(defmacro eval-after-init! (&rest body)
  "Evaluate BODY after init file is loaded.
If the first argument is `once', runs BODY only once."
  `(eval-on-hook! after-init-hook ,@body))

(defmacro defadvice! (where symbol name arglist &optional docstring &rest body)
  "Define an advice called NAME and add it to function named SYMBOL.

This is primarily just a more terse syntax for `define-advice'."
  (declare (doc-string 5) (indent 4))
  (unless (stringp docstring)
    (push docstring body)
    (setq docstring nil))
  (let ((advice-sym (intern (format "%s@%s" symbol name))))
    `(progn
       (defun ,advice-sym ,arglist ,docstring ,@body)
       (advice-add ',symbol ,where #',advice-sym))))

(defmacro undefadvice! (symbol name &optional undef)
  "Removes the advice named NAME from SYMBOL.
If UNDEF is non-nil, unbounds NAME as well."
  (let ((advice-sym (intern (format "%s@%s" symbol name))))
    (if (not undef)
        `(advice-remove ',symbol #',name)
      `(progn
         (fmakunbound #',name)
         (advice-remove ',symbol #',name)))))

(provide 'base-utils)
