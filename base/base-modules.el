;; -*- lexical-binding: t -*-
;; Module loader for the Emacs config

(defun parse-module (module)
  "Parse module MODULE and return an associative pair
containing the category and name."
  (condition-case err
      (cons
       (substring (symbol-name (car module)) 1 nil)
       (symbol-name (cdr module)))
    ('error
     (error "Invalid module: '%s %s'\n%s" (car module) (cdr module) err))))

(defun get-module-init (module)
  "Get entry point of MODULE if it exists.
Entry points can be either of the formats name.el or name/init.el.
Returns `nil` if no entry point is found."
  (let* ((parsed-module (parse-module module))
         (module-prefix
          (format "%s%s/%s"
                  config-modules-dir
                  (car parsed-module)
                  (cdr parsed-module))))
    (cond
     ((file-exists-p (concat module-prefix ".el"))
      (concat module-prefix ".el"))
     ((and (file-directory-p module-prefix)
           (file-exists-p (concat module-prefix "/init.el")))
      (concat module-prefix "/init.el"))
     (t
      (error "Module ':%s %s' not found"
             (car parsed-module)
             (cdr parsed-module))))))

(defun load-module (module)
  "Load MODULE and return `t` if the loading succeeds, return `nil` otherwise."
  (load (get-module-init module) nil t))

(defun parse-modules-list (modules)
  "Parse modules list into list of category and name pairs."
  (let ((category nil) (value))
    (cl-dolist (elem modules value)
      (cond
       ((keywordp elem)
        (setq category elem))
       (category
        (add-to-list 'value (cons category elem)))
       (t
        (error "Invalid modules list")
        (cl-return))))))

(defmacro load-modules! (&rest modules)
  "Loads modules provided in the arguments and initializes the config."
  `(when-let (modules-list (parse-modules-list (quote ,modules)))
     (mapcar #'load-module modules-list)))

(provide 'base-modules)

