;; -*- lexical-binding: t -*-
;; Base editor configuration

(defvar config-leader-key "SPC"
  "Leader key for evil-mode.")

(defvar config-alt-leader-key "M-SPC"
  "Alternate leader key used in Insert and Emacs state.")

(show-paren-mode) ;; Highlight matching parens
(electric-indent-mode t) ;; Auto-indentation
(electric-pair-mode t) ;; Auto-close pairs

(provide 'base-editor)
