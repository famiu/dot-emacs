;; -*- lexical-binding: t -*-
;; Basic UI configuration

(defvar display-line-numbers-exempt-modes nil
  "Modules to disable display-line-numbers in.")

(defvar after-load-theme-hook nil
  "Hook run after a color theme is loaded using `load-theme'.")

(defadvice! :around load-theme
            load-theme-advice (orig theme &optional no-confirm no-enable)
  "Run `after-load-theme-hook` on `load-theme` and disable previously enabled themes."
  (when-let (result (funcall orig theme no-confirm no-enable))
    (unless no-enable
      ;; Disable previously enabled themes
      (mapc #'disable-theme (remq theme custom-enabled-themes))
      ;; Run load-theme hooks
      (run-hooks 'after-load-theme-hook))
    result))

;; all-the-icons
(use-package all-the-icons
  :config
  ;; Automatically install the all-the-icons fonts if not already installed

  (unless noninteractive
    (eval-after-init!
     
     (unless (member "all-the-icons" (font-family-list))
       (all-the-icons-install-fonts t)))))

;; Solaire mode
(use-package solaire-mode
  ;; Ensure solaire-mode is running in all solaire-mode buffers
  :hook (change-major-mode . turn-on-solaire-mode)
  ;; ...if you use auto-revert-mode, this prevents solaire-mode from turning
  ;; itself off every time Emacs reverts the file
  :hook (after-revert . turn-on-solaire-mode)
  ;; To enable solaire-mode unconditionally for certain modes:
  :hook (ediff-prepare-buffer . solaire-mode)
  ;; Highlight the minibuffer when it is activated:
  :hook (minibuffer-setup . solaire-mode-in-minibuffer)
  :config
  ;; The bright and dark background colors are automatically swapped the first 
  ;; time solaire-mode is activated. Namely, the backgrounds of the `default` and
  ;; `solaire-default-face` faces are swapped. This is done because the colors 
  ;; are usually the wrong way around. If you don't want this, you can disable it:
  (setq solaire-mode-auto-swap-bg nil)

  ;; Reload solaire-mode after loading theme
  (add-hook 'after-load-theme-hook #'solaire-global-mode))

;; Disable unnecessary UI elements
(scroll-bar-mode 0)
(menu-bar-mode 0)
(tool-bar-mode 0)

;; Line numbers
(eval-after-init!
 (defun display-line-numbers--turn-on ()
   "Turn on line numbers except for certain major modes.
Exempt major modes are defined in `display-line-numbers-exempt-modes'."
   (unless (or (minibufferp)
               (member major-mode display-line-numbers-exempt-modes))
     (display-line-numbers-mode)))

 (global-display-line-numbers-mode t))

(provide 'base-ui)
