;; -*- lexical-binding: t -*-
;; Emacs Lisp language configuration

(with-eval-after-load 'elisp-mode
  (use-package ielm)
  
  (use-package eldoc
    :hook
    ((emacs-lisp-mode lisp-interaction-mode ielm-mode) . eldoc-mode)

    :config
    (defadvice! :around elisp-get-var-docstring
                append-value (orig sym)
      "Display variable value next to documentation in ElDoc."
      (when-let (ret (funcall orig sym))
        (if (boundp sym)
            (concat ret " "
                    (let* ((truncated " [...]")
                           (print-escape-newlines t)
                           (str (symbol-value sym))
                           (str (prin1-to-string str))
                           (limit (- (frame-width) (length ret) (length truncated) 1)))
                      (format (format "%%0.%ds%%s" (max limit 0))
                              (propertize str 'face 'warning)
                              (if (< (length str) limit) "" truncated))))
          ret)))

    (defadvice! :around elisp-get-fnsym-args-string
                append-docstring (orig &rest args)
      "Add first line of docstring to ElDoc's function information."
      (when-let ((ret (apply orig args))
                 (doc (elisp--docstring-first-line (documentation (car args) t)))
                 (w (frame-width))
                 (color-doc (propertize doc 'face 'font-lock-comment-face)))
        (when (and doc (not (string= doc "")))
          (setq ret (concat ret " " color-doc))
          (when (> (length doc) w)
            (setq ret (substring ret 0 (1- w)))))
        ret)))

  (use-package highlight-defined
    :hook
    ((emacs-lisp-mode lisp-interaction-mode ielm-mode) . highlight-defined-mode)

    :config
    (setq highlight-defined-face-use-itself t))

  (use-package elisp-def
    :hook
    ((emacs-lisp-mode ielm-mode) . elisp-def-mode)))
