;; -*- lexical-binding: t -*-
;; EVIL-mode configuration

(use-package evil
  :init
  (use-package goto-chg)
  (use-package undo-fu)

  ;; Settings
  (setq evil-want-integration t
        evil-want-keybinding nil
        evil-toggle-key "C-z"
        evil-want-C-u-delete t
        evil-want-C-u-scroll t
        evil-want-Y-yank-to-eol t
        evil-search-module 'evil-search
        evil-respect-visual-line-mode t
        evil-split-window-below t
        evil-vsplit-window-right t
        evil-want-fine-undo t
        evil-undo-system 'undo-fu) 
  
  :config
  ;; Start evil mode
  (evil-mode 1)

  (use-package evil-collection
    :custom
    (evil-collection-setup-minibuffer t)
    (evil-collection-calendar-want-org-bindings t)
    :config
    (evil-collection-init))

  (use-package evil-surround
    :config
    (global-evil-surround-mode 1))

  (use-package evil-commentary
    :config
    (evil-commentary-mode))

  (use-package evil-exchange
    :config
    (evil-exchange-install))

  (use-package evil-numbers
    :config
    (define-key evil-normal-state-map (kbd "<kp-add>") 'evil-numbers/inc-at-pt)
    (define-key evil-normal-state-map (kbd "<kp-subtract>") 'evil-numbers/dec-at-pt))

  (use-package evil-snipe
    :config
    (evil-snipe-mode +1)
    (evil-snipe-override-mode +1)))
