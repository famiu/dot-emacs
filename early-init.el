;; -*- lexical-binding: t -*-
;; Disable package.el
(setq package-enable-at-startup nil)

;; Set emacs config directory to parent directory of this file
(setq user-emacs-directory (file-name-directory load-file-name))
